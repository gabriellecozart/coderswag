//
//  ProductCell.swift
//  coder-swag
//
//  Created by Gabrielle Cozart on 3/16/18.
//  Copyright © 2018 Gabrielle Cozart. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productPrice: UILabel!
}
