//
//  CategoryCell.swift
//  coder-swag
//
//  Created by Gabrielle Cozart on 3/16/18.
//  Copyright © 2018 Gabrielle Cozart. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
    
    func updateView(category: Category) {
        categoryImage.image = UIImage(named: category.imageName)
        categoryTitle.text = category.title
    }

}
